#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>

#include "librpc.h"
#include "sassi.h"

void print_raw(void* data_in, size_t total_size, size_t per_line);

int main(void)
{
    uint64_t timeout_us = 5*1000*1000; // 5 seconds
    while(1) {
        printf("For SET_INTEGRATION_TIME, press 1\nFor GET_INTEGRATION_TIME, press 2\nFor PERFORM_INTEGRATION, press 3\n===========================\n> ");
        int choice = 0;
        scanf("%d", &choice);
        switch(choice) {
            case 1:;
            {
                uint64_t micros = 0;
                printf("Enter time in micro seconds: ");
                scanf("%"PRIu64"", &micros);
                if(rpc_send_uint64(PSD_RPC_SOCKET, PSD_OP_SET_INTEGRATION_MICRO_S, micros, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 2:;
            {
                uint64_t micros = 0;
                if(rpc_recv_uint64(PSD_RPC_SOCKET, PSD_OP_GET_INTEGRATION_MICRO_S, &micros, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("Time: %"PRIu64"!\n", micros);
                }
            }
            break;
            case 3:;
            {
                if(rpc_send_uint8(PSD_RPC_SOCKET, PSD_OP_PERFORM_INTEGRATION, 1, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            default:;
        }
    }
}

void print_raw(void* data_in, size_t total_size, size_t per_line)
{
    uint8_t* data = data_in;
    for(size_t i = 0; i < total_size; i += per_line) {
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            printf("%02x ", data[i + j]);
        }
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            uint8_t d = data[i + j];
            if(32 <= d && d <= 127) {
                printf("%c ", (char)d);
            } else {
                printf(". ");
            }
        }
        printf("\n");
    }
}

