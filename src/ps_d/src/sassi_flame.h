#ifndef SASSI_FLAME_H
#define SASSI_FLAME_H

#include <stdint.h>

_Pragma("GCC diagnostic push")
_Pragma("GCC diagnostic ignored \"-Wattributes\"")
struct flame_elem_fs {
    uint16_t flame_temp_pre;
    uint8_t flame_header[16];
    uint16_t flame_spectrum[2048];
    uint16_t end;
    uint16_t flame_temp_post;
    uint32_t time_s;
} __attribute__((packed));

typedef struct flame_elem_fs flame_elem_fs;
_Pragma("GCC diagnostic pop")

int sassi_flame_init(char *path);
int sassi_flame_loop(flame_elem_fs* elem, uint64_t flame_integration_micro_s);
int sassi_flame_close(void);

#endif
