/**
 * \file sassi.c
 * \brief SASSI^2 Flame spectrometer serial driver
 *
 * Has no encoded size, and the length of the payload depends heavily
 * on the commands, so we're going to use libtty for this
 */

#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/time.h>
#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "libtty.h"
#include "libdebug.h"
#include "powerboard.h"
#include "sassi_flame.h"

static int write_(int fd, const void* data, size_t len);
static int read_(int fd, void *data, size_t len);
static int read_ack(void);
static int write_cmd(const char* cmd);
static int write_cmd_param(const char* cmd, uint16_t param);

static int sassi_flame_fd;

int sassi_flame_init(char *path)
{
    { // Start ing 9600 baud, change to 4800
        startSerial(&sassi_flame_fd, path, B9600, 1, 10);
        fcntl(sassi_flame_fd, F_SETFL, fcntl(sassi_flame_fd, F_GETFL, 0) | O_NONBLOCK);
        tcflush(sassi_flame_fd, TCIOFLUSH);
        if(write_cmd("aA")) {
            P_ERR_STR("Failed sending aA");
            close(sassi_flame_fd);
            return EXIT_FAILURE;
        }
        if(write_cmd("K1\n")) {
            P_ERR_STR("Failed sending K1\\n");
            close(sassi_flame_fd);
            return EXIT_FAILURE;
        }
        close(sassi_flame_fd);
        sassi_flame_fd = -1;
        usleep(60*1000);
    }
    { // Open again in 4800
        startSerial(&sassi_flame_fd, path, B4800, 1, 10);
        fcntl(sassi_flame_fd, F_SETFL, fcntl(sassi_flame_fd, F_GETFL, 0) | O_NONBLOCK);
        tcflush(sassi_flame_fd, TCIOFLUSH);
        if(write_cmd("K1\n")) {
            P_ERR_STR("Failed sending KA\\n");
            close(sassi_flame_fd);
            return EXIT_FAILURE;
        }
        usleep(1000*1000);
        tcflush(sassi_flame_fd, TCIOFLUSH); // Eat the ASCII ack
    }
    return write_cmd("bB");
}

static int write_(int fd, const void* data, size_t len)
{
    if(write(fd, data, len) != (ssize_t)len){
        P_ERR("Can't send data: %d", errno);
        return -1;
    }
    tcdrain(fd);
    return (int)len;
}

static int read_(int fd, void *data, size_t len)
{
    if(read(fd, data, len) == -1){
        P_ERR("Can't read data: %d", errno);
        return -1;
    }
    P_INFO_STR("Read the following");
    return (int)len;
}

/**
 * 1. Set integration time based on discrete brackets of altitude
 * 2. Fetch the raw spectrum
 * 3. Fetch temperature sensor count
 * 4. Read all temperature sensors
 *
 * We also try to power cycle it, since why not and it helps
 * reliability if we have a clean slate (and I'm out of ideas)
 */

static int sassi_flame_loop_temp(uint16_t *temp)
{
    for(int i = 0;i < 3;i++){
        tcflush(sassi_flame_fd, TCIOFLUSH);
        write_(sassi_flame_fd, "t", 1);
        usleep(1000*1000);
        uint16_t temp_tmp;
        read_(sassi_flame_fd, &temp_tmp, 2);
        // NOTES
        // 1. Endian is wrong here but is corrected on the ground
        *temp = temp_tmp; 
        P_INFO("Flame Temp: %d", (int)(*temp));
    }
    return EXIT_SUCCESS;
}

int sassi_flame_loop(flame_elem_fs *elem, uint64_t flame_integration_micro_s)
{
    if(!elem) {
        P_ERR_STR("Got NULL elem");
        return EXIT_FAILURE;
    }
    tcflush(sassi_flame_fd, TCIOFLUSH);

    sassi_flame_loop_temp(&(elem->flame_temp_pre));
    
    // Set integration time
    P_INFO("Using an integration time of %zu milliseconds", flame_integration_micro_s/1000);
    write_cmd_param("I", (uint16_t)(flame_integration_micro_s/1000));
    
    // Immediately send all data
    write_cmd_param("M", 0); 
    write_cmd_param("P", 0);

    // Disregard any ACKs the Flame sent us
    tcflush(sassi_flame_fd, TCIOFLUSH);

    // Collect spectra, immediately send down serial line
    write_(sassi_flame_fd, "S", 1);
    tcdrain(sassi_flame_fd);
    sleep(10 + (unsigned int)flame_integration_micro_s/1000000); // Wait for spectrometer to integrate

    // Read off header (TODO: parse)
    read_(sassi_flame_fd, elem->flame_header, 1); // ACK nonsense (?)
    read_(sassi_flame_fd, elem->flame_header, 16); // header information
    P_INFO_STR("Header information we have so far:");
    usleep(1000*1000);
    
    // Read off actual spectrum (there is junk afterwards as well)
    int in_size = read_(
            sassi_flame_fd, elem->flame_spectrum, sizeof(elem->flame_spectrum));
    if(in_size != sizeof(elem->flame_spectrum)){
        P_ERR_STR("Can't read spectrum");
    }
    sassi_flame_loop_temp(&(elem->flame_temp_post));
    elem->end = 0xFDFF;
    elem->time_s = (uint32_t)time(NULL);
    return EXIT_SUCCESS;
}

int sassi_flame_close(void)
{
    closeSerial(sassi_flame_fd);
    return EXIT_SUCCESS;
}

static int read_ack(void)
{
    uint8_t buffer[256];
    const ssize_t read_val = read(sassi_flame_fd, buffer, 256);
    if(read_val > 1) {
        P_ERR_STR("We have more data than what we asked for");
        return EXIT_SUCCESS;
    } else if(read_val == 1) {
        if(buffer[0] == 6) {
            P_INFO_STR("FlameIO ACKed");
            return EXIT_SUCCESS;
        } else if(buffer[0] == 21) {
            P_ERR_STR("FlameIO NACKed");
        }
    } else if(read_val == -1) {
        P_ERR("read failed, errno: %d (%s)", errno, strerror(errno));
    } else if(read_val == 0) {
        P_ERR("no response, errno: %d (%s)", errno, strerror(errno));
    } else {
        P_ERR("No idea wtf is going on, %d", (int)read_val);
    }
    return EXIT_FAILURE;
}

static int write_cmd(const char* cmd)
{
    P_INFO("Attempting to send command %s", cmd);
    usleep(1000*1000);
    tcflush(sassi_flame_fd, TCIOFLUSH);
    write(sassi_flame_fd, cmd, strlen(cmd));
    tcdrain(sassi_flame_fd);
    usleep(1000*1000);
    return read_ack();
}

static int write_cmd_param(const char* cmd, uint16_t param)
{
    P_INFO("Attempting to send command %s with parameter %d", cmd, param);
    write_(sassi_flame_fd, cmd, 1);
    uint16_t param_flip = htobe16(param);
    write_(sassi_flame_fd, &param_flip, 2);
    tcdrain(sassi_flame_fd);
    usleep(1000*1000);
    return read_ack();
}

