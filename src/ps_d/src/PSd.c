#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "librpc.h"
#include "liblog.h"
#include "libdebug.h"
#include "libdaemon.h"
#include "powerboard.h"
#include "sassi_flame.h"
#include "sassi.h"

static rpc_server_fs rpc_server;
static log_fs flame_log;
static log_rpc_shim_fs log_shim = {
    .logs = (log_fs*[1]) {
        &flame_log,
    },
    .num_logs = 1
};

static flame_elem_fs flame_elem;

static uint8_t flame_auto_integration = 0;
static uint64_t flame_integration_micro_s = 0;
static uint8_t flame_perform_integration = 0;

static int turned_on_PIB = 0;

static int psd_loop_flame_spectrum(void)
{
    if(flame_auto_integration) {
        int above = 0;
        int below = 0;

        const int flame_len = sizeof(flame_elem.flame_spectrum)/sizeof(flame_elem.flame_spectrum[0]);
        for(int i = 0;i < flame_len;i++) {
            above += flame_elem.flame_spectrum[i] > 0.80*65536.0;
            below += flame_elem.flame_spectrum[i] < 0.20*65536.0;
        }

        if(above > 0.20*flame_len) {
            if(flame_integration_micro_s > 500*1000)
                flame_integration_micro_s -= 500*1000;
            else
                flame_integration_micro_s = 1*1000;
        }else if(below < 0.80*flame_len) {
            flame_integration_micro_s += 500*1000;
        }
        flame_perform_integration = 1;
    }
    return EXIT_SUCCESS;
} 

static int psd_rpc_get_spectrum(uint8_t *in, size_t in_size, uint8_t *out, size_t *out_size, void *param)
{
    (void)in; (void)in_size; (void)param;
    if(*out_size < 1440*sizeof(uint16_t)) {
        P_ERR("out size too small, %zu", *out_size);
        return EXIT_FAILURE;
    }
    memcpy(out, flame_elem.flame_spectrum, 1440*sizeof(uint16_t));
    *out_size = sizeof(uint16_t)*1440;
    return EXIT_SUCCESS;
}

static int psd_init(void)
{
    if(rpc_server_init(&rpc_server, PSD_RPC_SOCKET, 0) == EXIT_FAILURE) {
        P_ERR_STR("Can't initialize RPC");
        return EXIT_FAILURE;
    }
    rpc_server_add_elem(&rpc_server, RPC_OP_SPLIT_LOG, log_rpc_shim, &log_shim, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_SET_INTEGRATION_MODE, rpc_rpc_set_uint8, &flame_auto_integration, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_GET_INTEGRATION_MODE, rpc_rpc_get_uint8, &flame_auto_integration, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_GET_INTEGRATION_MICRO_S, rpc_rpc_get_uint64, &flame_integration_micro_s, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_SET_INTEGRATION_MICRO_S, rpc_rpc_set_uint64, &flame_integration_micro_s, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_GET_TEMP, rpc_rpc_get_uint16, &flame_elem.flame_temp_pre, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_GET_SPECTRUM, psd_rpc_get_spectrum, NULL, NULL);
    rpc_server_add_elem(&rpc_server, PSD_OP_PERFORM_INTEGRATION, rpc_rpc_set_uint8, &flame_perform_integration, NULL);

    uint32_t mask = HOTSWAP_PIB1_CH1 | HOTSWAP_PIB1_CH3;
    if(turned_on_PIB == 0) {
        mask |= HOTSWAP_PIB1;
    }
    if(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_EN, mask, PBD_OP_WAIT)) {
        P_ERR_STR("Can't query powerboard to enable PIB");
#ifdef NDEBUG
        return EXIT_FAILURE;
#endif
    }
    turned_on_PIB = 1;

    P_DEBUG_STR("Wait 10 seconds for flame to startup");
    sleep(10);

    if(sassi_flame_init(PSD_SERIAL) == EXIT_FAILURE) {
        P_ERR_STR("Can't initialize SASSI FLAME");
        return EXIT_FAILURE;
    }
    
    if(log_init(&flame_log, PSD_BIN, 1, LOG_FORMAT_U8, sizeof(flame_elem_fs), LOG_MODE_NATIVE, 8, 0) == EXIT_FAILURE) {
        P_ERR_STR("Cannot initialize log");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int psd_close(void)
{
    if(sassi_flame_close()) {
        P_ERR_STR("Failed to close sassi");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// NOTE: there is a discrepency with the lengths of the headers
// Currently everything works fine, but we aren't reading all the
// data we should
int main(int argc, char **argv)
{
    (void)argc; (void)argv;
    daemonize();
    if(psd_init() == EXIT_FAILURE) {
        P_ERR_STR("Can't initialize PSd");
        return EXIT_FAILURE;
    }
    // Do we haev any commands to create for PSd?
    P_INFO_STR("Entering main loop");
    while(1) {
        usleep(100*1000);
        rpc_server_loop(&rpc_server); 
        psd_loop_flame_spectrum();

    	// Gaurding variable so that parameters can be set before integration begins
        if(!flame_perform_integration) continue;

        flame_perform_integration = 0;

        if(sassi_flame_loop(&flame_elem, flame_integration_micro_s) == EXIT_FAILURE) {
    	  P_ERR_STR("Can't pull spectra");
    	  continue;
    	}

        P_DEBUG_STR("Finished reading flame spectra");

        /*
        for(size_t i = 0;i < sizeof(flame_elem.flame_spectrum)/sizeof(flame_elem.flame_spectrum[0]);i++) {
            if(flame_elem.flame_spectrum[i] == 0) {
                P_ERR("First zero spectra was %d, this should at least be above 1000", i);
                if(i > 1000) {
                    break;
                }
            }
        }
        */
    	if(log_add(&flame_log, &flame_elem) == EXIT_FAILURE) {
    	  P_ERR_STR("Can't append element to Flame log");
    	  continue;
    	}
        P_DEBUG_STR("Finished logging flame spectra");
    }
    psd_close();
    return 0;
}
