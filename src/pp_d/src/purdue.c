#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>

#include "libtty.h"
#include "liblog.h"
#include "librpc.h"
#include "powerboard.h"
#include "purdue.h"

#define RPC_WAIT (1*1000*1000) // 1 second

#define PURDUE_DATA_LENGTH (17)
#define LENGTH_WITH_PADDING_NO_CRC (17)

static int purdue_serial_fd;

static rpc_server_fs rpc_server;
static log_fs purdue_data; 
static log_rpc_shim_fs log_shim = {
    .logs = (log_fs*[1]) {
        &purdue_data,
    },
    .num_logs = 1
};

static int turned_on_PIB = 0;

static int enable_hotswaps(void)
{
    uint32_t mask = HOTSWAP_PIB1_CH2 | HOTSWAP_PIB1_CH3;
    if(turned_on_PIB == 0) {
        mask |= HOTSWAP_PIB1;
    }
    while(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_EN, mask, RPC_WAIT) == EXIT_FAILURE) {
        syslog(LOG_ERR, "PPd failed to enable its hotswap, sleeping and retrying");
        sleep(2);
    }
    turned_on_PIB = 1;
    return EXIT_SUCCESS;
}

static int disable_hotswaps(void)
{
    return rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, HOTSWAP_PIB1_CH2, RPC_WAIT);
}

int purdue_init(char *serial){
    if(rpc_server_init(&rpc_server, PPD_RPC_SOCKET, RPC_SERVER_MT)) {
        P_ERR_STR("Failed to start RPC server");
        return EXIT_FAILURE;
    }

    rpc_server_add_elem(&rpc_server, RPC_OP_SPLIT_LOG, log_rpc_shim, &log_shim, NULL);
    
    startSerial(&purdue_serial_fd, serial, B9600, 1, 20);

    log_init(&purdue_data, PPD_BIN, 1, LOG_FORMAT_U8, LENGTH_WITH_PADDING_NO_CRC, LOG_MODE_NATIVE, 8, 0);
    return EXIT_SUCCESS;
}

int purdue_loop(){
    enable_hotswaps();
    
    usleep(1000*1000);

    tcflush(purdue_serial_fd, TCIOFLUSH);
    if(write(purdue_serial_fd, "Q", 1) != 1) {
        P_ERR("Failed to write \"Q\", errno: %d (%s)", errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }
    tcdrain(purdue_serial_fd);

    uint8_t buffer[128];
    
    // disable_hotswaps(); Bad readings?
    
    if(read(purdue_serial_fd, buffer, PURDUE_DATA_LENGTH) == -1) {
        P_ERR("Couldn't read, errno: %d (%s)", errno, strerror(errno));
        errno = 0;
        return EXIT_FAILURE;
    }
    
    uint8_t output[LENGTH_WITH_PADDING_NO_CRC];

    // Cmd Response
    output[0] = buffer[0];

    // Heat flux (3-byte endian swap), 0 pad
    output[1] = buffer[3];
    output[2] = buffer[2];
    output[3] = buffer[1];
    output[4] = 0;

    // Thermocouple (3-byte endian swap), 0 pad
    output[5] = buffer[6];
    output[6] = buffer[5];
    output[7] = buffer[4];
    output[8] = 0;

    // Cold junction (2-byte endian swap)
    output[9] = buffer[8];
    output[10] = buffer[7];

    // Micropirani A/B/C (each is 2-byte endian swap)
    output[11] = buffer[10];
    output[12] = buffer[9];
    output[13] = buffer[12];
    output[14] = buffer[11];
    output[15] = buffer[14];
    output[16] = buffer[13];

    log_add(&purdue_data, output);
    return EXIT_SUCCESS;
}

int purdue_close(){
    closeSerial(purdue_serial_fd);
    return EXIT_SUCCESS;
}

