#include <unistd.h>
#include <stdlib.h>

#include "libdaemon.h"
#include "purdue.h"

int main(void) {
    daemonize();
    purdue_init(PPD_SERIAL);
    while(1) {
        purdue_loop();
        sleep(3);
    }
    purdue_close();
    return EXIT_SUCCESS;
}
