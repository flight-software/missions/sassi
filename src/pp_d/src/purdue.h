#ifndef PURDUE_H
#define PURDUE_H

#define PPD_BIN "pp-d"

#define PPD_RPC_SOCKET (50400)

#define PPD_SERIAL "/dev/ttyO5"

int purdue_init(char *serial);
int purdue_loop(void);
int purdue_close(void);

#endif
