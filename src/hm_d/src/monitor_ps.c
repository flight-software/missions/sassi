#include <pthread.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>

#include "libdaemon.h"
#include "librpc.h"
#include "powerboard.h"
#include "sassi.h"
#include "libadcs.h"
#include "libhmd.h"
#include "eyestar_d2.h"
#include "purdue.h"
#include "mission_ops.h"
#include "monitor_pp.h"
#include "monitor_ps.h"

//==========================================================================================================================

static SASSI_CONFIG sassi_config = {
#ifdef NDEBUG
    .DETUMBLE_WAIT_TIME_SEC = 352 * 60,
    .HOLD_TX_INTERVAL_SEC = 60 * 60,
    .STUCK_TIME_SEC = 2 * 60 * 60,
    .STUCK_RADIO_ON_SEC = 15 * 60,
#else
    .DETUMBLE_WAIT_TIME_SEC = 60,
    .HOLD_TX_INTERVAL_SEC = 90,
    .STUCK_TIME_SEC = 3 * 60,
    .STUCK_RADIO_ON_SEC = 2 * 60,
#endif
#ifdef NDEBUG
    .POWER_CHANGE_WAIT_TIME_SEC = 3 * 60,
#else
    .POWER_CHANGE_WAIT_TIME_SEC = 15,
#endif
    .POWER_CHANGE_AMP_DOUBLE = 0.050,
    .FLAME_INTEGRATION_TIME_MICROSEC = 65 * 1000 * 1000,
    .FLAME_INTEGRATION_TIME_SEC = 0,
    .FLAME_EMERGENCY_STOP_TIME_SEC = 2100,
#ifdef NDEBUG
    .FLAME_TX_INTERVAL_TIME_SEC = 35 * 60,
    .FLAME_MAX_INTEGRATION_COUNT = 5,
    .FLAME_INTEGRATION_INTERVAL_WAIT_SEC = 280,
#else
    .FLAME_TX_INTERVAL_TIME_SEC = 2 * 60,
    .FLAME_MAX_INTEGRATION_COUNT = 2,
    .FLAME_INTEGRATION_INTERVAL_WAIT_SEC = 60
#endif
};

static SASSI_STATUS sassi_status = {
    .current_state = SASSI_UNKNOWN,
    .timer = 0,
    .last_hold_tx_time = 0,
    .flame_last_live_check = 0,
    .stuck_time = 0,
    .stuck_radio_on_time = 0,
    .missed_flame_tx = 0,
    .sent_detumble_data = 0
};

rpc_server_fs monitor_ps_rpc_server;
static unsigned int FLAME_STARTUP_WAIT_TIME = 15;

static pid_t* flame_pid;
static pid_t* globalstar_pid;

//==========================================================================================================================

static int psThreadSetCurrentState(SASSI_STATE current_state_)
{
    P_DEBUG("Moving state from %d to %d", sassi_status.current_state, current_state_);
    sassi_status.current_state = current_state_;
    return EXIT_SUCCESS;
}

void* ps_thread_main(void* arg)
{
    hmd_thread_arg_fs* args = arg;
    {
        int r;
        if((r = pthread_mutex_lock(&args->thread_lock))) {
            P_ERR("Could not lock mutex, errno: %d (%s)", r, strerror(r));
            return NULL;
        }
    }
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push(pthread_cleanup, NULL);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    startupRPCServer();

    flame_pid = &args->pid;
    globalstar_pid = malloc(sizeof(pid_t));
    *globalstar_pid = -1;

    if(hmd_thread_register(PPD_BIN, pp_thread_main, NULL)) {
        P_ERR_STR("Can't register purdue thread");
        return NULL;
    }

    if(is_daemon_alive(PSD_BIN) == 0) {
        P_INFO("psThreadMain started, PSD_BIN was already alive. We must have been restarted. Read pid from %s and state from %s and config from %s", HMD_PSD_PID_FILE, HMD_PSD_STATUS_FILE, HMD_PSD_CONFIG_FILE);
        if(retrieveStatus(&sassi_status)) {
            P_ERR_STR("Failed to retrieve current SASSI status from file, will deduce from global star and power");
            psThreadSetCurrentState(SASSI_UNKNOWN);
        }
        if(retrieveConfig(&sassi_config)) {
            P_ERR_STR("Failed to retrieve current SASSI config from file. This is serious and we need this to be reuploaded.");
        }
        if(receiveFlamePidAndSanityCheck(flame_pid)) {
            P_ERR_STR("Failed to get pid from flame, restart");
            killFlameDaemon();
            sleep(FLAME_STARTUP_WAIT_TIME);
        }
    } else {
        retrieveStatus(&sassi_status);
        retrieveConfig(&sassi_config);
    }
    
    if(sassi_status.current_state == SASSI_UNKNOWN) {
        // We have not determined that the current state is anything else, so this must be a fresh boot
        psThreadSetCurrentState(SASSI_DETUMBLE);
        int i = 0;
        while(recordStatus(&sassi_status) && i < 5) {
            P_ERR("Trying to store SASSI status %d/5", ++i);
            sleep(1);
        }
    }
    
    time_t loop_started = time(NULL);
    while(1) {
        time_t now = time(NULL);
        time_t diff = now - loop_started;
        if(diff < 30) sleep((unsigned int)(30 - diff));
        loop_started = now;
        pthread_testcancel();

        time_t current_time = time(NULL);

        sassi_config.FLAME_INTEGRATION_TIME_SEC = (time_t)(sassi_config.FLAME_INTEGRATION_TIME_MICROSEC / (1000 * 1000));

        switch(sassi_status.current_state) {
            case SASSI_DETUMBLE:;
            {
                P_LOGIC("Current State: %s", "SASSI_DETUMBLE");
                P_LOGIC_STR("Disabling Purdue");
                if(sassi_status.timer == 0) {
                    sassi_status.timer = current_time;
                    sleep(60); // Make sure systems are up and running
                }
                P_DEBUG("CURRENT_TIME - (timer + DETUMBLE) = %d", (int)(current_time - (sassi_status.timer + sassi_config.DETUMBLE_WAIT_TIME_SEC)));
                uint8_t exceeded_time = current_time > (sassi_status.timer + sassi_config.DETUMBLE_WAIT_TIME_SEC);
                if(exceeded_time) {
                    sassi_status.timer = 0;
                    sassi_status.sent_detumble_data = 0;
                    sassi_status.last_hold_tx_time = time(NULL);
                    psThreadSetCurrentState(SASSI_HOLD);
                }
            }
            break;
            case SASSI_HOLD:;
            {
                P_LOGIC("Current State: %s", "SASSI_HOLD");
                P_LOGIC_STR("Disabling Purdue");
                uint8_t time_for_tx = current_time > (sassi_config.HOLD_TX_INTERVAL_SEC + sassi_status.last_hold_tx_time);
                if(time_for_tx || sassi_status.sent_detumble_data == 0) {
                    globalstarLoop(globalstar_pid);
                    if(sassi_status.sent_detumble_data == 0) {
                        P_DEBUG_STR("WE LETS TRANSMIT 2 SUBSYSTEMS BITCHES");
                        uint8_t subsys[2] = {SUBSYS_CADH, SUBSYS_ADCS};
                        if(rpc_send_buf(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, subsys, 2, RADIO_TX_WAIT_TIME_USEC)) {
                            // damn
                            P_ERR_STR("WE FAILED TO DO RSD_OP_SCIENCE_TX");
                            sassi_status.sent_detumble_data = 0;
                            sassi_status.last_hold_tx_time = 0;
                        } else {
                            P_ERR_STR("WE SUCCEEDED TO DO RSD_OP_SCIENCE_TX");
                            sassi_status.sent_detumble_data = 1;
                            sassi_status.last_hold_tx_time = time(NULL);
                        }
                    } else {
                        P_DEBUG_STR("WE LETS TRANSMIT 1 SUBSYSTEM BITCHES");
                        uint8_t subsys[1] = {SUBSYS_CADH};
                        if(rpc_send_buf(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, subsys, 1, RADIO_TX_WAIT_TIME_USEC) == EXIT_SUCCESS) {
                            sassi_status.last_hold_tx_time = time(NULL);
                        }
                    }
                    P_ERR_STR("LETS ASK IF WE SHOULD GO NOMINAL");
                    uint64_t move_on = RADIO_STAY_HOLD;
                    if(rpc_recv_uint64(RSD_MISSION_RPC_SOCKET, RSD_OP_BEGIN_NOMINAL, &move_on, RADIO_TX_WAIT_TIME_USEC) == EXIT_FAILURE){
                        P_ERR_STR("Cannot query globalstar for phase");
                    } else if(move_on == RADIO_BEGIN_NOMINAL) {
                        P_INFO_STR("Moving to either Sunlight or Eclipse");
                        uint8_t power_above_threshold = 0;
                        if(abovePowerThreshold(&power_above_threshold)) {
                            P_ERR_STR("Failed to read ADCS voltage. What do now?");
                        }
                        sassi_status.stuck_time = 0;
                        sassi_status.timer = 0;
                        sassi_status.last_hold_tx_time = 0;
                        psThreadSetCurrentState(power_above_threshold ? SASSI_SUNLIGHT : SASSI_ECLIPSE);
                        P_LOGIC_STR("Moving to nominals ops");
                        P_LOGIC_STR("Enabling Purdue");
                        if(hmd_thread_enable(PPD_BIN)) {
                            P_ERR_STR("Failed to enable pp-d thread");
                        }
                    }
                    killGlobalstarDaemon();
                }
            }
            break;
            case SASSI_ECLIPSE:;
            {
                P_LOGIC("Current State: %s", "SASSI_ECLIPSE");
                check_purdue();
                handle_stuck_logic(globalstar_pid);
                uint8_t exceeded_time = current_time > (sassi_status.timer + sassi_config.POWER_CHANGE_WAIT_TIME_SEC);
                uint8_t power_below_threshold = 0;
                if(belowPowerThreshold(&power_below_threshold)) {
                    P_ERR_STR("Failed to get voltages from ADCS");
                } else if(!power_below_threshold || sassi_status.timer == 0) {
                    sassi_status.timer = current_time;
                } else if(power_below_threshold && exceeded_time) {
                    psThreadSetCurrentState(SASSI_ACTIVE_COLLECTION);
                    sassi_status.stuck_time = 0;
                    sassi_status.timer = 0;
                    killGlobalstarDaemon(); // just in case
                }
            }
            break;
            case SASSI_ACTIVE_COLLECTION:;
            {
                P_LOGIC("Current State: %s", "SASSI_ACTIVE_COLLECTION");
                check_purdue();
                if(sassi_status.timer == 0) {
                    sassi_status.timer = (current_time - sassi_config.POWER_CHANGE_WAIT_TIME_SEC); // The time cutoffs here are based on when we entered the eclipse, not after waiting 3 minutes
                }
                rpc_send_uint8(ACD_RPC_SOCKET, ADCS_OP_SET_CONTROLS, 0, 10000000);
                
                int exceeded_time = 0;
                int integrations = 0;
                int time_left_for_int = 1;
                while(!exceeded_time                                           &&
                       integrations < sassi_config.FLAME_MAX_INTEGRATION_COUNT &&
                       time_left_for_int) {
                    flameLoop(flame_pid);

                    if(rpc_send_uint64(PSD_RPC_SOCKET, PSD_OP_SET_INTEGRATION_MICRO_S, sassi_config.FLAME_INTEGRATION_TIME_MICROSEC, 10000000)) {
                        P_ERR_STR("psThreadMain failed to set flame integration time");
                    } else if(rpc_send_uint8(PSD_RPC_SOCKET, PSD_OP_PERFORM_INTEGRATION, 1, 10000000)) {
                        P_ERR_STR("psThreadMain failed to set perform integration flag");
                    }
                    sleep((unsigned int)(sassi_config.FLAME_INTEGRATION_TIME_SEC + sassi_config.FLAME_INTEGRATION_INTERVAL_WAIT_SEC));
                    time_t right_now = time(NULL);
                    exceeded_time = right_now > (sassi_status.timer + sassi_config.FLAME_EMERGENCY_STOP_TIME_SEC);
                    time_left_for_int = ((sassi_status.timer + sassi_config.FLAME_EMERGENCY_STOP_TIME_SEC) - right_now) > sassi_config.FLAME_INTEGRATION_TIME_SEC;
                    integrations += 1;
                }
                killFlameDaemon();
                sassi_status.timer = 0;
                rpc_send_uint8(ACD_RPC_SOCKET, ADCS_OP_SET_CONTROLS, 1, 10000000);
                psThreadSetCurrentState(SASSI_SUNLIGHT);
            }
            break;
            case SASSI_SUNLIGHT:;
            {
                P_LOGIC("Current State: %s", "SASSI_SUNLIGHT");
                check_purdue();
                handle_stuck_logic(globalstar_pid);
                uint8_t exceeded_time = current_time > (sassi_status.timer + sassi_config.POWER_CHANGE_WAIT_TIME_SEC);
                uint8_t power_above_threshold = 0;
                if(abovePowerThreshold(&power_above_threshold)) {
                    P_ERR_STR("Failed to get voltages from ADCS");
                } else if(!power_above_threshold || sassi_status.timer == 0) {
                    sassi_status.timer = current_time;
                } else if(power_above_threshold && exceeded_time) {
                    psThreadSetCurrentState(SASSI_ACTIVE_TX);
                    sassi_status.stuck_time = 0;
                    sassi_status.timer = 0;
                    killGlobalstarDaemon(); // just in case
                }
            }
            break;
            case SASSI_ACTIVE_TX:;
            {
                P_LOGIC("Current State: %s", "SASSI_ACTIVE_TX");
                check_purdue();
                // if timer == 0, its our first tx
                // if time(NULL) > (timer + FLAME_TX_INTERVAL_TIME_SEC), then its our second tx
                uint8_t exceeded_time = current_time > (sassi_status.timer + sassi_config.FLAME_TX_INTERVAL_TIME_SEC);
                if(sassi_status.timer == 0 || exceeded_time) {
                    globalstarLoop(globalstar_pid);
                    sassi_status.timer = current_time;
                    if(exceeded_time) {
                        if(sassi_status.missed_flame_tx) {
                            uint8_t subsys[4] = {SUBSYS_CADH, SUBSYS_FLAME, SUBSYS_ADCS, SUBSYS_PSP};
                            rpc_send_buf(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, subsys, 4, RADIO_TX_WAIT_TIME_USEC);
                        } else {
                            uint8_t subsys[3] = {SUBSYS_CADH, SUBSYS_ADCS, SUBSYS_PSP};
                            rpc_send_buf(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, subsys, 3, RADIO_TX_WAIT_TIME_USEC);
                        }
                        sassi_status.missed_flame_tx = 0;
                        psThreadSetCurrentState(SASSI_ECLIPSE);
                        sassi_status.timer = 0;
                    } else {
                        if(rpc_trigger_action(RSD_MISSION_RPC_SOCKET, RSD_OP_SCIENCE_TX, RADIO_TX_WAIT_TIME_USEC)) {
                            P_ERR_STR("Failed to trigger active tx for first round");
                            sassi_status.missed_flame_tx = 1;
                        } else {
                            sassi_status.missed_flame_tx = 0;
                        }
                    }
                    killGlobalstarDaemon();
                }
            }
            break;
            default:;
            {
                P_LOGIC_STR("Disabling Purdue");
                if(hmd_thread_disable(PPD_BIN)) {
                    P_ERR_STR("Failed to disable pp-d thread");
                }
            }
        }

        recordStatus(&sassi_status); // This happens often enough that if it errors out each time, we have other problems
        recordConfig(&sassi_config);
    }
    pthread_cleanup_pop(1);
}

//==========================================================================================================================

static void startupRPCServer(void)
{
    rpc_server_init(&monitor_ps_rpc_server, PSD_THREAD_RPC_SOCKET, RPC_SERVER_MT); // MUST BE MT else TX will get stuck in a dependency loop between RSD_BIN and this thread
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_DETUMBLE_WAIT_TIME_SEC, rpc_rpc_set_int64, &sassi_config.DETUMBLE_WAIT_TIME_SEC, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_POWER_CHANGE_WAIT_TIME_SEC, rpc_rpc_set_int64, &sassi_config.POWER_CHANGE_WAIT_TIME_SEC, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_POWER_CHANGE_AMP_DOUBLE, rpc_rpc_set_double, &sassi_config.POWER_CHANGE_AMP_DOUBLE, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_FLAME_INTEGRATION_TIME_MICROSEC, rpc_rpc_set_uint64, &sassi_config.FLAME_INTEGRATION_TIME_MICROSEC, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_FLAME_EMERGENCY_STOP_TIME_SEC, rpc_rpc_set_int64, &sassi_config.FLAME_EMERGENCY_STOP_TIME_SEC, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_FLAME_TX_INTERVAL_TIME_SEC, rpc_rpc_set_int64, &sassi_config.FLAME_TX_INTERVAL_TIME_SEC, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_CONFIG_FLAME_MAX_INTEGRATION_COUNT, rpc_rpc_set_uint8, &sassi_config.FLAME_MAX_INTEGRATION_COUNT, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_REVERT_TO_HOLD, revertToHold, NULL, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_GET_STAGE, rpc_rpc_get_uint32, &sassi_status.current_state, NULL);
    rpc_server_add_elem(&monitor_ps_rpc_server, HMD_SASSI_SET_STAGE, rpc_rpc_set_uint32, &sassi_status.current_state, NULL);
}

//==========================================================================================================================

static int receiveFlamePidAndSanityCheck(pid_t* pid)
{
    if(rpc_recv_buf(PSD_RPC_SOCKET, RPC_OP_GET_PID, pid, sizeof(*pid), 10000000)) {
        P_ERR_STR("psThreadMain timed out waiting for ps-d PID");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int killFlameDaemon(void)
{
    if(system("pkill "PSD_BIN)) {
        P_ERR_STR("psThreadMain detected that ps-d was running but it failed to pkill it, continuing...");
    }
    if(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, HOTSWAP_PIB1_CH1, 5*1000*1000)) {
        P_ERR_STR("psThreadMain failed to kill Flame hotswap");
        return EXIT_FAILURE;
    }
    sassi_status.flame_last_live_check = 0;
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int retrieveStatus(SASSI_STATUS* status)
{
    SASSI_STATUS local_status;
    FILE* fp = NULL;
    fp = fopen(HMD_PSD_STATUS_FILE, "r");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open status file for reading, err is: %d (%s)", errno, strerror(errno));
        goto retrieveConfigBackup;
    }
    if(fread(&local_status, sizeof(local_status), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to read status from file");
        fclose(fp);
        goto retrieveConfigBackup;
    }
    *status = local_status;
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close status file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
retrieveConfigBackup:
    fp = fopen(HMD_PSD_STATUS_FILE".bak", "r");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open status file for reading, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fread(&local_status, sizeof(local_status), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to read status from file");
    }
    *status = local_status;
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close status file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int recordStatus(SASSI_STATUS* status)
{
    rename(HMD_PSD_STATUS_FILE, HMD_PSD_STATUS_FILE".bak");
    FILE* fp = NULL;
    fp = fopen(HMD_PSD_STATUS_FILE, "w");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open status file for writing, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fwrite(status, sizeof(*status), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to write status to file");
    }
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close status file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int retrieveConfig(SASSI_CONFIG* config)
{
    SASSI_CONFIG local_config;
    FILE* fp = NULL;
    fp = fopen(HMD_PSD_CONFIG_FILE, "r");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open config file for reading, err is: %d (%s)", errno, strerror(errno));
        goto retrieveConfigBackup;
    }
    if(fread(&local_config, sizeof(local_config), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to read config from file");
        fclose(fp);
        goto retrieveConfigBackup;
    }
    *config = local_config;
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close config file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
retrieveConfigBackup:
    fp = fopen(HMD_PSD_CONFIG_FILE".bak", "r");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open config file for reading, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fread(&local_config, sizeof(local_config), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to read config from file");
    }
    *config = local_config;
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close config file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
    
//==========================================================================================================================

static int recordConfig(SASSI_CONFIG* config)
{
    rename(HMD_PSD_CONFIG_FILE, HMD_PSD_CONFIG_FILE".bak");
    FILE* fp = NULL;
    fp = fopen(HMD_PSD_CONFIG_FILE, "w");
    if(fp == NULL) {
        P_ERR("psThreadMain failed to open config file for writing, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    if(fwrite(config, sizeof(*config), 1, fp) != 1) {
        P_ERR_STR("psThreadMain failed to write config to file");
    }
    if(fclose(fp)) {
        P_ERR("psThreadMain failed to close config file, err is: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int getPanelCurrents(double* currents)
{
    if(rpc_recv_buf(ACD_RPC_SOCKET, ADCS_OP(ALL, FC, CURRENT), currents, PBD_LEN_PACK * sizeof(*currents), 10000000) == EXIT_FAILURE) {
        P_ERR_STR("Failed to get ADCS currents via rpc for psThreadMain");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int abovePowerThreshold(uint8_t* result)
{
    double currents[4] = {0.0};
    int _error = getPanelCurrents(currents);
    if(_error) {
        *result = 0;
        return _error;
    }
    for(size_t i = 0; i < 4; i++) {
        if(currents[i] > sassi_config.POWER_CHANGE_AMP_DOUBLE) {
            *result = 1;
            return EXIT_SUCCESS;
        }
    }
    *result = 0;
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int belowPowerThreshold(uint8_t* result)
{
    uint8_t aboveResult = 0;
    int _error = abovePowerThreshold(&aboveResult);
    *result = !aboveResult;
    return _error;
}

//==========================================================================================================================

static void flameLoop(pid_t* pid)
{
    while(is_daemon_alive(PSD_BIN) != 0) {
        // PSD_BIN is not alive
        if(startup_daemon(PSD_BIN)) {
            P_ERR_STR("psThreadMain failed to start ps-d");
        } else {
            sleep(FLAME_STARTUP_WAIT_TIME);
            if(receiveFlamePidAndSanityCheck(pid)) {
                P_ERR_STR("We expected a flame pid ping, but did not get one. Restarting ps-d");
                killFlameDaemon();
            }
        }
        sleep(FLAME_STARTUP_WAIT_TIME);
    }
    
    if(shouldCheckAlive(sassi_status.flame_last_live_check)) {
        while(is_daemon_alive(PSD_BIN) != 0) {
            P_ERR_STR("psThreadMain detected that ps-d has exited, restarting...");
            // PSD_BIN is not alive
            if(startup_daemon(PSD_BIN)) {
                P_ERR_STR("psThreadMain failed to start ps-d");
            } else {
                sleep(FLAME_STARTUP_WAIT_TIME);
                if(receiveFlamePidAndSanityCheck(pid)) {
                    P_ERR_STR("We expected a flame pid ping, but did not get one. Restarting ps-d");
                    killFlameDaemon();
                }
                if(hmd_write_pid_to_file(HMD_PSD_PID_FILE, *pid)) {
                    P_ERR_STR("Failed to write PSD_BIN pid to a file");
                }
            }
            sleep(FLAME_STARTUP_WAIT_TIME);
        }
        sassi_status.flame_last_live_check = time(NULL);
    }
}

//==========================================================================================================================

static int revertToHold(uint8_t* in, size_t in_size, uint8_t* out, size_t* out_size, void* param)
{
    if(out_size) *out_size = 0;
    (void)in; (void)in_size; (void)out; (void)param; // ignore
    killFlameDaemon();
    sassi_status.timer = 0;
    sassi_status.flame_last_live_check = 0;
    sassi_status.current_state = SASSI_HOLD;
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static int disableRadioHotswaps(void)
{
    P_ERR_STR("DISABLE RSD HOTSWAP");
    uint8_t i = 0;
    while(rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, HOTSWAP_J11, PBD_OP_WAIT) && i < 5) {
        P_ERR("Could not kill global star radio %d/5", ++i);
    }
    if(i >= 5) {
        P_ERR_STR("Failed to kill global star radio hotswap");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static void globalstarLoop(pid_t* pid)
{
    int i = 0;
gsloop_tryagain:
    if(i == 5) return;
    P_INFO("%s startup attemp %d/5", RSD_BIN, i);
    while(is_daemon_alive(RSD_BIN) != 0) {
        P_ERR("%s is NOT alive", RSD_BIN);
        if(startup_daemon(RSD_BIN)) {
            P_ERR_STR( "rsThreadMain failed to start rs-d");
            i += 1;
            sleep(5);
            goto gsloop_tryagain;
        } else {
            sleep(5);
            if(receiveGlobalstarPidAndSanityCheck(pid)) {
                P_ERR_STR("We expected a globalstar pid ping, but did not get one. Restarting rs-d");
                killGlobalstarDaemon();
            }
        }
    }
    if(receiveGlobalstarPidAndSanityCheck(pid)) {
        P_ERR_STR("Timed out getting globalstar pid");
        killGlobalstarDaemon();
    } else {
        P_DEBUG_STR("Going to sleep!");
        sleep(70); // let radio startup
        P_DEBUG_STR("Woke up!");
    }
}

//==========================================================================================================================

static int receiveGlobalstarPidAndSanityCheck(pid_t* pid)
{
    if(rpc_recv_buf(RSD_RPC_SOCKET, RPC_OP_GET_PID, pid, sizeof(*pid), 10*1000*1000)) {
        P_ERR_STR("psThreadMain timed out waiting for rs-d PID");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//==========================================================================================================================

static void killGlobalstarDaemon(void)
{
    P_ERR_STR("ENTERED KILL GLOBALSTAR DAEMON");
    /*
    if(rpc_trigger_action(RSD_RPC_SOCKET, RSD_OP_PROCESS_QUEUE, 120*1000000)) { // 2 minutes
        // Failed or timed out
        P_ERR_STR("Call to process queue either failed or timed out");
    }*/
    if(is_daemon_alive(RSD_BIN) == 0) system("pkill "RSD_BIN);
    P_ERR_STR("KILLED RSD DAEMON HARSH");
    disableRadioHotswaps();
}

//==========================================================================================================================

static void pthread_cleanup(void* arg)
{
    (void)arg;
    rpc_server_close(&monitor_ps_rpc_server);
    killFlameDaemon();
    killGlobalstarDaemon();
}

//==========================================================================================================================

static int shouldCheckAlive(time_t last_check)
{
    return time(NULL) - last_check > 30;
}

//==========================================================================================================================

static void check_purdue(void)
{
    int r = 0;
    if(hmd_thread_check(PPD_BIN, &r)) {
        P_ERR("pp-d thread check failed because, errno: %d (%s)", r, strerror(r));
        P_ERR_STR("Let's re-enable Purdue");
        if(hmd_thread_enable(PPD_BIN)) {
            P_ERR_STR("Also failed to enable the pp-d thread");
            if(hmd_thread_deregister(PPD_BIN)) {
                P_ERR_STR("Also failed to deregister pp-d");
            }
            if(hmd_thread_register(PPD_BIN, pp_thread_main, NULL)) {
                P_ERR_STR("Failed to register pp-d, wait until called again");
                return;
            }
            if(hmd_thread_enable(PPD_BIN)) {
                P_ERR_STR("Failed to enable thread after re-registering, try again later");
                return;
            }
        }
    }
    P_DEBUG_STR("pp-d thread is good to go");
}

//==========================================================================================================================

static void handle_stuck_logic(pid_t* radio_pid)
{
    if(sassi_status.stuck_time == 0 && sassi_status.stuck_radio_on_time == 0) {
        sassi_status.stuck_time = time(NULL);
        return;
    }
    time_t now = time(NULL);
    if(sassi_status.stuck_radio_on_time > 0) {
        if((now - sassi_status.stuck_radio_on_time) > sassi_config.STUCK_RADIO_ON_SEC) {
            killGlobalstarDaemon();
            sassi_status.stuck_radio_on_time = 0;
            sassi_status.stuck_time = 0;
            return;
        }
        return;
    }
    if((now - sassi_status.stuck_time) > sassi_config.STUCK_TIME_SEC) {
        globalstarLoop(radio_pid);
        sassi_status.stuck_radio_on_time = time(NULL);
        sassi_status.stuck_time = 0;
    }
}

