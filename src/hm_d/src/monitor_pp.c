#include <stdint.h>
#include <pthread.h>

#include "libdaemon.h"
#include "librpc.h"
#include "liblog.h"
#include "libdebug.h"
#include "powerboard.h"
#include "purdue.h"
#include "libhmd.h"
#include "monitor_pp.h"

static int turn_off_purdue(void);
static void pthread_cleanup(void* arg);

void* pp_thread_main(void* arg)
{
    hmd_thread_arg_fs* args = arg;
    {
        int r = 0;
        if((r = pthread_mutex_lock(&args->thread_lock))) {
            P_ERR("Could not lock mutex, errno: %d (%s)", r, strerror(r));
            return NULL;
        }
    }
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
    pthread_cleanup_push(pthread_cleanup, NULL);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    if(is_daemon_alive(PPD_BIN)) {
        P_ERR_STR("Purdue was not up, let's do that");
        startup_daemon(PPD_BIN);
        sleep(5);
    }
    while(rpc_recv_buf(PPD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), 5*1000*1000)) {
        P_ERR_STR("Failed to ask purdue for its PID");
        if(is_daemon_alive(PPD_BIN)) {
            P_ERR_STR("Purdue was not up, let's do that");
            startup_daemon(PPD_BIN);
            sleep(5);
        }
    }
    P_INFO("Got pp-d pid: %d", (int)args->pid);
    while(1) {
        pthread_testcancel();
        if(is_daemon_alive(PPD_BIN)) {
            P_ERR_STR("pp-d must have died, restart it");
            startup_daemon(PPD_BIN);
            sleep(5);
            while(rpc_recv_buf(PPD_RPC_SOCKET, RPC_OP_GET_PID, &args->pid, sizeof(args->pid), 5*1000*1000)) {
                P_ERR_STR("Failed to ask purdue for its PID");
                if(is_daemon_alive(PPD_BIN)) {
                    P_ERR_STR("Purdue was not up, let's do that");
                    startup_daemon(PPD_BIN);
                    sleep(5);
                }
            }
        } else {
            sleep(10);
        }
    }
    pthread_cleanup_pop(1);
}

static int turn_off_purdue(void)
{
    system("pkill "PPD_BIN);
    rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, HOTSWAP_PIB1_CH2, 10*1000*1000);
    return EXIT_SUCCESS;
}

static void pthread_cleanup(void* arg)
{
    (void)arg;
    turn_off_purdue();
}

