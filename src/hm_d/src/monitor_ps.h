#ifndef _MONITOR_PS_H_
#define _MONITOR_PS_H_

#include <time.h>
#include <stdint.h>

#include "libdebug.h"
#include "sassi.h"
#include "eyestar_d2.h"
#include "libhmd.h"

#define PSD_THREAD_RPC_SOCKET (50102)

#define SUBSYS_FLAME UINT8_C('F') // Flame
#define SUBSYS_ADCS  UINT8_C('A') // ADCS
#define SUBSYS_PSP   UINT8_C('P') // Purdue
#define SUBSYS_CADH  UINT8_C('H') // Health monitor

#define GLOBALSTAR_STARTUP_WAIT_TIME (5)

#define HMD_SASSI_CONFIG_DETUMBLE_WAIT_TIME_SEC          0x01
#define HMD_SASSI_CONFIG_POWER_CHANGE_WAIT_TIME_SEC      0x02
#define HMD_SASSI_CONFIG_POWER_CHANGE_AMP_DOUBLE         0x03
#define HMD_SASSI_CONFIG_FLAME_INTEGRATION_TIME_MICROSEC 0x04
#define HMD_SASSI_CONFIG_FLAME_EMERGENCY_STOP_TIME_SEC   0x05
#define HMD_SASSI_CONFIG_FLAME_TX_INTERVAL_TIME_SEC      0x06
#define HMD_SASSI_CONFIG_FLAME_MAX_INTEGRATION_COUNT     0x07
#define HMD_SASSI_REVERT_TO_HOLD                         0x08
#define HMD_SASSI_GIVE_BEACON                            0x09
#define HMD_SASSI_GET_STAGE                              0x0A
#define HMD_SASSI_SET_STAGE                              0x0B

#define HMD_PSD_CONFIG_FILE HMD_LOG_DIR"/"PSD_BIN"-config"
#define HMD_PSD_STATUS_FILE HMD_LOG_DIR"/"PSD_BIN"-status"
#define HMD_PSD_PID_FILE HMD_PID_FILE(PSD_BIN)
#define HMD_RSD_PID_FILE HMD_PID_FILE(RSD_BIN)

typedef enum {
    SASSI_UNKNOWN,
    SASSI_DETUMBLE,
    SASSI_HOLD,
    SASSI_ECLIPSE,
    SASSI_ACTIVE_COLLECTION,
    SASSI_SUNLIGHT,
    SASSI_ACTIVE_TX
} SASSI_STATE;

typedef struct {
    time_t DETUMBLE_WAIT_TIME_SEC;
    time_t HOLD_TX_INTERVAL_SEC;
    time_t STUCK_TIME_SEC;
    time_t STUCK_RADIO_ON_SEC;
    time_t POWER_CHANGE_WAIT_TIME_SEC;
    double POWER_CHANGE_AMP_DOUBLE;
    uint64_t FLAME_INTEGRATION_TIME_MICROSEC;
    time_t FLAME_INTEGRATION_TIME_SEC;
    time_t FLAME_EMERGENCY_STOP_TIME_SEC;
    time_t FLAME_TX_INTERVAL_TIME_SEC;
    time_t FLAME_INTEGRATION_INTERVAL_WAIT_SEC;
    uint8_t FLAME_MAX_INTEGRATION_COUNT;
} SASSI_CONFIG; // I'm not really concerned about padding or anything

typedef struct {
    SASSI_STATE current_state;
    time_t timer;
    time_t flame_last_live_check;
    time_t stuck_time;
    time_t stuck_radio_on_time;
    time_t last_hold_tx_time;
    uint8_t missed_flame_tx;
    uint8_t sent_detumble_data;
} SASSI_STATUS;

extern rpc_server_fs monitor_ps_rpc_server;

void* ps_thread_main(void* arg);
static void startupRPCServer(void);
static int receiveFlamePidAndSanityCheck(pid_t* pid);
static int killFlameDaemon(void);
static int retrieveStatus(SASSI_STATUS* status);
static int recordStatus(SASSI_STATUS* status);
static int retrieveConfig(SASSI_CONFIG* config);
static int recordConfig(SASSI_CONFIG* config);
static int abovePowerThreshold(uint8_t* result);
static int belowPowerThreshold(uint8_t* result);
static void flameLoop(pid_t* pid);
static int revertToHold(uint8_t*, size_t, uint8_t*, size_t*, void*);
static int disableRadioHotswaps(void);
static void globalstarStartup(pid_t* pid);
static void globalstarLoop(pid_t* pid);
static int receiveGlobalstarPidAndSanityCheck(pid_t* pid);
static void killGlobalstarDaemon(void);
static void pthread_cleanup(void* arg);
static int shouldCheckAlive(time_t last_check);
static int getPanelCurrents(double* currents);
static void check_purdue(void);
static void handle_stuck_logic(pid_t* radio_pid);

#endif
