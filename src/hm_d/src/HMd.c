#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>

#include "powerboard.h"
#include "libadcs.h"
#include "libdebug.h"
#include "libdaemon.h"
#include "librpc.h"
#include "liblog.h"
#include "libhmd.h"
#include "sassi.h"
#include "purdue.h"
#include "monitor_ps.h"
#include "monitor.h"

static hmd_sassi_beacon_packed_fs sassi_hmd_beacon = {0};
static rpc_server_fs rpc_server;
static pthread_mutex_t beacon_lock = PTHREAD_MUTEX_INITIALIZER;
static log_fs beacon_log;


static void hmd_pbd_copy(hmd_sassi_pbd_beacon_packed_fs* dest, const hmd_pbd_default_beacon_fs* src);
static void hmd_adcs_copy(hmd_sassi_adcs_beacon_packed_fs* dest, const hmd_adcs_default_beacon_fs* src);
static int monitor_rpc_init(void);
static void delete_syslog(void);
static void sig_handler(int a, siginfo_t* b, void* c);
static void setup_signal_handler(void);

int main(void)
{
    daemonize();
    P_INFO_STR("Startup up hm-d");
    if(monitor_rpc_init()) {
        P_ERR_STR("Failed to start rpc server");
        return EXIT_FAILURE;
    }
    if(hmd_thread_init()) {
        P_ERR_STR("Failed to init system monitor");
        return EXIT_FAILURE;
    }
    if(hmd_thread_enable_defaults(1, 1)) {
        P_ERR_STR("Failed to enable default powerboard and ADCS threads");
        return EXIT_FAILURE;
    }
    if(hmd_thread_register(PSD_BIN, ps_thread_main, NULL)) {
        P_ERR_STR("Couldn't make ps-d thread");
        return EXIT_FAILURE;
    }
    if(hmd_thread_enable(PSD_BIN)) {
        P_ERR_STR("Couldn't enable ps-d thread");
        return EXIT_FAILURE;
    }
    if(log_init(&beacon_log, HMD_BIN, 1, LOG_FORMAT_U8, sizeof(sassi_hmd_beacon), LOG_MODE_NATIVE, 1, 10*1000)) {
        P_ERR_STR("Failed to initialize beacon log");
        return EXIT_FAILURE;
    }
    if(log_modify_overwrite(&beacon_log, 1)) {
        P_ERR_STR("Failed to mark beacon log as overwrite");
        return EXIT_FAILURE;
    }
    sassi_hmd_beacon.sync = 0xECEB;
    sassi_hmd_beacon.host_id = 2;
    memset(sassi_hmd_beacon.custom_data, 0, sizeof(sassi_hmd_beacon.custom_data));
    hmd_pbd_default_beacon_fs pbd_default = {0};
    hmd_adcs_default_beacon_fs adcs_default = {0};
    while(1) {
        if(hmd_thread_check(PBD_BIN, NULL)) {
            P_ERR_STR("pb-d thread crashed");
            if(hmd_thread_enable(PBD_BIN)) {
                P_ERR_STR("Faild to enable pb-d");
            }
        } else {
            if(hmd_pbd_thread_get_beacon(&pbd_default)) {
                P_ERR_STR("Failed to get pb-d beacon");
            } else {
                pthread_mutex_lock(&beacon_lock);
                hmd_pbd_copy(&sassi_hmd_beacon.pbd, &pbd_default);
                pthread_mutex_unlock(&beacon_lock);
            }
        }
        if(hmd_thread_check(ACD_BIN, NULL)) {
            P_ERR_STR("adcs thread crashed");
            if(hmd_thread_enable(ACD_BIN)) {
                P_ERR_STR("Faield to enable adcs");
            }
        } else {
            if(hmd_adcs_thread_get_beacon(&adcs_default)) {
                P_ERR_STR("Failed to get ac-d beacon");
            } else {
                pthread_mutex_lock(&beacon_lock);
                hmd_adcs_copy(&sassi_hmd_beacon.adcs, &adcs_default);
                pthread_mutex_unlock(&beacon_lock);
            }
        }
        sassi_hmd_beacon.system_time = (uint32_t)time(NULL);
        log_add(&beacon_log, &sassi_hmd_beacon);
        delete_syslog();
        sleep(10);
    }
}

static void hmd_pbd_copy(hmd_sassi_pbd_beacon_packed_fs* dest, const hmd_pbd_default_beacon_fs* src)
{
    for(size_t i = 0; i < PBD_LEN_PACK; i++) {
        dest->battery_voltage[i] = (float)src->battery_voltage[i];
    }
    memcpy(dest->battery_temp, src->battery_temp, sizeof(dest->battery_temp));
}

static void hmd_adcs_copy(hmd_sassi_adcs_beacon_packed_fs* dest, const hmd_adcs_default_beacon_fs* src)
{
    for(size_t i = 0; i < 4; i++) {
        dest->quaternions[i] = (float)src->quaternions[i];
    }
    for(size_t i = 0; i < 3; i++) {
        dest->rates[i] = (float)src->rates[i];
    }
    dest->illumination_state = src->illumination_state;
    dest->algorithm = src->algorithm;
}

static int monitor_rpc_init(void)
{
    if(rpc_server_init(&rpc_server, HMD_RPC_SOCKET, RPC_SERVER_MT)) {
        P_ERR_STR("Can't start rpc");
        return EXIT_FAILURE;
    }
    rpc_server_add_elem(&rpc_server, HMD_RPC_OP_SET_CUSTOMDATA, rpc_rpc_set_uint32, (uint32_t*)sassi_hmd_beacon.custom_data, NULL);
    return EXIT_SUCCESS;
}

static void delete_syslog(void)
{
#if defined CROSS_COMPILE && defined NDEBUG
    static time_t last_delete = 0;
    time_t now = time(NULL);
    if(now - last_delete > 60) {
        system("rm /var/log/messages.*");
        system("rm /var/log/cdh/logs/pb-d*");
        last_delete = now;
    }
#endif
}

static void sig_handler(int a, siginfo_t* b, void* c)
{
    (void)a; (void)b; (void)c;
    close(rpc_server.udp_fd);
    close(monitor_ps_rpc_server.udp_fd);
    P_ERR_STR("hm-d signal handler called");
    exit(EXIT_FAILURE);
}

static void setup_signal_handler(void)
{
    struct sigaction s = {0};
    s.sa_sigaction = sig_handler;
    s.sa_flags = SA_SIGINFO;
    sigaction(SIGUSR1, &s, NULL);
    sigaction(SIGUSR2, &s, NULL);
    sigaction(SIGHUP, &s, NULL);
    sigaction(SIGINT, &s, NULL);
    sigaction(SIGQUIT, &s, NULL);
    sigaction(SIGABRT, &s, NULL);
    sigaction(SIGSEGV, &s, NULL);
    sigaction(SIGPIPE, &s, NULL);
    sigaction(SIGTERM, &s, NULL); 
}

