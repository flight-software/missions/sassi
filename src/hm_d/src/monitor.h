#ifndef _SRC_HMD_MONITOR_H_
#define _SRC_HMD_MONITOR_H_

#include <stdint.h>
#include <time.h>

#include "powerboard.h"

#define HMD_RPC_OP_SET_CUSTOMDATA (0x01)
#define HMD_RPC_OP_GET_BEACON     (0x02)

_Pragma("GCC diagnostic push")
_Pragma("GCC diagnostic ignored \"-Wpacked\"")
_Pragma("GCC diagnostic ignored \"-Wattributes\"")
struct hmd_sassi_pbd_beacon_packed_fs {
    float battery_voltage[PBD_LEN_PACK];
    float battery_temp[PBD_LEN_PACK];
} __attribute__((packed));

typedef struct hmd_sassi_pbd_beacon_packed_fs hmd_sassi_pbd_beacon_packed_fs;

struct hmd_sassi_adcs_beacon_packed_fs {
    float quaternions[4];
    float rates[3];
    uint16_t illumination_state;
    uint8_t algorithm;
} __attribute__((packed));

typedef struct hmd_sassi_adcs_beacon_packed_fs hmd_sassi_adcs_beacon_packed_fs;

struct hmd_sassi_beacon_packed_fs {
    uint16_t sync;
    uint8_t host_id;
    uint32_t system_time;
    uint8_t custom_data[4];
    hmd_sassi_adcs_beacon_packed_fs adcs;
    hmd_sassi_pbd_beacon_packed_fs pbd;
} __attribute((packed));

typedef struct hmd_sassi_beacon_packed_fs hmd_sassi_beacon_packed_fs;
_Pragma("GCC diagnostic pop")

#endif
