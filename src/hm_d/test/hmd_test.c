#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>

#include "librpc.h"
#include "libhmd.h"
#include "monitor_ps.h"
#include "monitor.h"

void print_raw(void* data_in, size_t total_size, size_t per_line);

int main(void)
{
    uint64_t timeout_us = 5*1000*1000; // 5 seconds
    while(1) {
        printf("For CHANGE_DETUMBLE_WAIT_TIME, press 1\nFor CHANGE_POWER_WAIT_TIME, press 2\nFor CHANGE_POWER_THRESHOLD_AMPS, press 3\nFor CHANGE_FLAME_INTEGRATION_TIME, press 4\nFor CHANGE_FLAME_EMERGENCY_STOP, press 5\nFor CHANGE_FLAME_TX_INTERVAL, press 6\nFor CHANGE_FLAME_MAX_INT_COUNT, press 7\nFor REVERT_TO_HOLD, press 8\nFor GET_STATE, press 9\nFor SET_STATE, press 10\n===========================\n> ");
        int choice = 0;
        scanf("%d", &choice);
        switch(choice) {
            case 1:;
            {
                int64_t time;
                printf("Detumble wait time seconds: ");
                scanf("%"PRId64"", &time);
                if(rpc_send_int64(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_DETUMBLE_WAIT_TIME_SEC, time, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 2:;
            {
                int64_t time;
                printf("Power change wait time seconds: ");
                scanf("%"PRId64"", &time);
                if(rpc_send_int64(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_POWER_CHANGE_WAIT_TIME_SEC, time, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 3:;
            {
                double amps;
                printf("Power change threshold amps: ");
                scanf("%lf", &amps);
                if(rpc_send_double(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_POWER_CHANGE_AMP_DOUBLE, amps, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 4:;
            {
                uint64_t integration_time;
                printf("Integration time micro seconds: ");
                scanf("%"PRIu64"", &integration_time);
                if(rpc_send_uint64(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_FLAME_INTEGRATION_TIME_MICROSEC, integration_time, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 5:;
            {
                int64_t emergency_stop_time;
                printf("Emergency collection stop time seconds: ");
                scanf("%"PRId64"", &emergency_stop_time);
                if(rpc_send_int64(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_FLAME_EMERGENCY_STOP_TIME_SEC, emergency_stop_time, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 6:;
            {
                int64_t tx_interval_time;
                printf("TX interval time in seconds: ");
                scanf("%"PRId64"", &tx_interval_time);
                if(rpc_send_int64(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_FLAME_TX_INTERVAL_TIME_SEC, tx_interval_time, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 7:;
            {
                uint8_t max_int_count;
                printf("Flame max number integrations: ");
                scanf("%"PRIu8"", &max_int_count);
                if(rpc_send_uint8(PSD_THREAD_RPC_SOCKET, HMD_SASSI_CONFIG_FLAME_MAX_INTEGRATION_COUNT, max_int_count, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 8:;
            {
                if(rpc_trigger_action(PSD_THREAD_RPC_SOCKET, HMD_SASSI_REVERT_TO_HOLD, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    printf("SUCCESS!\n");
                }
            }
            break;
            case 9:;
            {
                SASSI_STATE s = SASSI_UNKNOWN;
                if(rpc_recv_uint32(PSD_THREAD_RPC_SOCKET, HMD_SASSI_GET_STAGE, &s, timeout_us)) {
                    printf("FAILED!\n");
                } else {
                    switch(s) {
                        case SASSI_UNKNOWN:;
                        {
                            printf("SASSI_UNKNOWN\n");
                        }
                        break;
                        case SASSI_DETUMBLE:;
                        {
                            printf("SASSI_DETUMBLE\n");
                        }
                        break;
                        case SASSI_HOLD:;
                        {
                            printf("SASSI_HOLD\n");
                        }
                        break;
                        case SASSI_ECLIPSE:;
                        {
                            printf("SASSI_ECLIPSE\n");
                        }
                        break;
                        case SASSI_SUNLIGHT:;
                        {
                            printf("SASSI_SUNLIGHT\n");
                        }
                        break;
                        case SASSI_ACTIVE_COLLECTION:;
                        {
                            printf("SASSI_ACTIVE_COLLECTION\n");
                        }
                        break;
                        case SASSI_ACTIVE_TX:;
                        {
                            printf("SASSI_ACTIVE_TX\n");
                        }
                        break;
                        default:;
                            printf("Got unknown %d", s);
                        break;
                    }
                }
            }
            break;
            case 10:;
            {
                printf("SASSI_UNKNOWN: 0\nSASSI_DETUMBLE: 1\nSASSI_HOLD: 2\nSASSI_ECLIPSE: 3\nSASSI_ACTIVE_COLLECTION: 4\nSASSI_ACTIVE_TX: 5\n> ");
                uint32_t state;
                scanf("%"PRIu32"", &state);
                SASSI_STATE s = state == 0 ? SASSI_UNKNOWN :
                                state == 1 ? SASSI_DETUMBLE :
                                state == 2 ? SASSI_HOLD :
                                state == 3 ? SASSI_ECLIPSE :
                                state == 4 ? SASSI_ACTIVE_COLLECTION :
                                state == 5 ? SASSI_SUNLIGHT :
                                state == 6 ? SASSI_ACTIVE_TX : 10000;
                if(s == 10000) {
                    printf("Bad value\n");
                } else {
                    if(rpc_send_int32(PSD_THREAD_RPC_SOCKET, HMD_SASSI_SET_STAGE, s, timeout_us)) {
                        printf("FAILED!\n");
                    } else {
                        printf("SUCCESS!\n");
                    }
                }
            }
            break;
            default:;
        }
    }
}

void print_raw(void* data_in, size_t total_size, size_t per_line)
{
    uint8_t* data = data_in;
    for(size_t i = 0; i < total_size; i += per_line) {
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            printf("%02x ", data[i + j]);
        }
        for(size_t j = 0; j < per_line; j++) {
            if(i + j >= total_size) break;
            uint8_t d = data[i + j];
            if(32 <= d && d <= 127) {
                printf("%c ", (char)d);
            } else {
                printf(". ");
            }
        }
        printf("\n");
    }
}

